import React, { Component } from 'react';

class Task extends React.Component {
  
  constructor(props) {
    super(props);
  }

  done = () => {
    this.props.done(this.props.todo);
  }

  render () {
    return <li onClick={this.done}>{this.props.todo}</li>
  }

}

export default Task;
