import React, { Component } from 'react';

// компоненты
import Task from './Task.js';

class Tasks extends React.Component {

  constructor(props) {
    super(props);
    this.state = { todos: this.props.todos, addString: '' };
  }

  handleChange = (event) => {
    this.setState({
      addString: event.target.value
    });
  }

  addTask = () => {
    var todos = this.props.todos;
    todos.push(this.state.addString);
    // this.setState({ });
    localStorage.setItem('todos', JSON.stringify(todos));
    this.setState({ todos: todos, addString: ''  });
  }

  doneTask = (todo) => {
    var todos = this.props.todos;
    todos.splice(todos.indexOf(todo), 1);
    localStorage.setItem('todos', JSON.stringify(todos));
    this.setState({ todos: todos });
  }

  render() {
    return (
      <div className=''>
        <ul>
        {
          this.state.todos.map( (todo) => {
            return <Task todo={todo} done={this.doneTask} />
          })
        }
        </ul>
        <input type="text" value={this.state.addString} onChange={this.handleChange} />
        <button onClick={this.addTask}>Add</button>
      </div>
    );
  }

}

export default Tasks;
