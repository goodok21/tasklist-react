import React, { Component } from 'react';

class Header extends React.Component {

  render() {
    return (
      <div className='container-header'>
        <h3>Tasks</h3>
        <a href='#' className='container-header-add_button'>+</a>
      </div>
    )
  }
}

export default Header;
