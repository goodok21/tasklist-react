import React, { Component } from 'react';

// CSS Styles
import './App.css';

// компоненты
import Tasks from './Tasks.js';
import Header from './Header.js';
import Footer from './Footer.js';

var todos = JSON.parse(localStorage.getItem('todos')) || [];

class App extends React.Component {
  render () {
    return (
      <div className='app'>
        <div className='container'>
          <Header />
          <Tasks todos = { todos } />
          <Footer num = { todos.length } />
        </div>
      </div>
    );
  }
}

export default App;
